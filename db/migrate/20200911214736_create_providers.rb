class CreateProviders < ActiveRecord::Migration[6.0]
  def change
    create_table :providers do |t|
      t.string :provider_type
      t.text :meta

      t.timestamps
    end
  end
end
