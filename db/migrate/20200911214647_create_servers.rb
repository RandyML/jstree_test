class CreateServers < ActiveRecord::Migration[6.0]
  def change
    create_table :servers do |t|
      t.text :meta
      t.references :provider, foreign_key: true

      t.timestamps
    end
  end
end
