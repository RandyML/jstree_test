class CreateApps < ActiveRecord::Migration[6.0]
  def change
    create_table :apps do |t|
      t.string :name
      t.text :meta
      t.references :server, null: false, foreign_key: true

      t.timestamps
    end
  end
end
