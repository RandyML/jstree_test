# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

digital_ocean = Provider.find_or_create_by(provider_type: 'DigitalOcean')
aws = Provider.find_or_create_by(provider_type: 'AWS')

servers = Server.create([
  {
    name: 'Servidor pruebas',
    provider: digital_ocean,
    state: 'Up and Running',
    url: 'app1.vivendo.com.mx',
    description: 'This is a simple description'
  },
  {
    name: 'Personal server',
    provider: digital_ocean,
    state: 'Up and Running',
    url: 'app1.vivendo.com.mx',
    description: 'This is a simple description'
  },
  {
    name: 'Testing 2',
    provider: aws,
    state: 'Down',
    url: 'testing.mysite.com',
    description: 'This is a simple description'
  },
  {
    name: 'Testing AWS',
    provider: aws,
    state: 'Up and Running',
    url: 'example.com',
    description: 'This is a simple description'
  },
])

apps = App.create([
  {
    name: 'my app',
    description: 'this is my app for the stuff I need',
    state: 'Running',
    prefix: 'blogonoro',
    server: servers[0]
  },
  {
    name: 'my app 2',
    description: 'another app for project',
    state: 'Disabled',
    prefix: 'argonaut',
    server: servers[1]
  },
  {
    name: 'test app for project keyno',
    description: 'another app again',
    state: 'Running',
    prefix: 'dromoi',
    server: servers[2]
  },
  {
    name: 'my app test 2',
    description: 'this is my app for the stuff I need',
    state: 'New',
    prefix: 'pregon',
    server: servers[3]
  },
  {
    name: 'test app',
    description: 'this is my app for the stuff I need',
    state: 'New',
    prefix: 'drobo',
    server: servers[1]
  }
])
