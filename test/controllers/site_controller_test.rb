require 'test_helper'

class SiteControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get site_index_url
    assert_response :success
  end

  test "should get docs" do
    get site_docs_url
    assert_response :success
  end

  test "should get faq" do
    get site_faq_url
    assert_response :success
  end

end
