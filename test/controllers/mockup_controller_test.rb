require 'test_helper'

class MockupControllerTest < ActionDispatch::IntegrationTest
  test "should get dashboard" do
    get mockup_dashboard_url
    assert_response :success
  end

  test "should get app_index" do
    get mockup_app_index_url
    assert_response :success
  end

  test "should get app_show" do
    get mockup_app_show_url
    assert_response :success
  end

  test "should get server_index" do
    get mockup_server_index_url
    assert_response :success
  end

  test "should get server_show" do
    get mockup_server_show_url
    assert_response :success
  end

  test "should get jstree" do
    get jstree_show_url
    assert_response :success
  end

end
