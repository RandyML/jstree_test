# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Requirements

* Ruby version

2.7.1

* Rails

6.0.3

* Sqlite3


## Getting started


This project uses sqlite3 ad DB, for starters.

The development of the models is still a Work In Progress, so everytime the project is downloaded the
DB should be regenerated, not migrated.

```
rm db/development.sqlite3
rails db:create
rails db:migrate
rails db:seed
```

This DB regeneration is because the model structures is changing too much. So having to create migration for this 
is not advisable.

