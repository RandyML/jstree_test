class App < ApplicationRecord
  belongs_to :server
  store :meta, accessors: [:description, :state, :prefix]
end
