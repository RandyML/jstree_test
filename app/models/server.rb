class Server < ApplicationRecord
  belongs_to :provider, optional: true
  store :meta, accessors: [:name, :url, :state, :description]
  validates :name, presence: true
end
