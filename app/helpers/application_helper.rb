module ApplicationHelper

  def nav_menu(option)
    content_for :title_and_separator, "#{option} | "
    content_for :nav do
      content_tag :div, class: "hidden sm:ml-6 sm:flex" do
        (nav_link 'Dashboard', home_dashboard_path, first: true, selected: (option == :dashboard)) +
        (nav_link 'Apps', apps_path, selected: (option == :apps)) +
        (nav_link 'Servers', servers_path, selected: (option == :servers)) +
        (nav_link 'Databases', '#', selected: (option == :databases))
      end
    end
  end

  def nav_link(name, path, first: false, selected: false)
    border_color = selected ? 'indigo-500' : 'transparent'
    left_margin_class = first ? '' : 'ml-8'
    link_to name, path, class: "#{left_margin_class} inline-flex items-center px-1 pt-1 border-b-2 border-#{border_color} text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
  end
  
  def description_card(title, description, &block)
    content_tag :div, class: "bg-white shadow overflow-hidden sm:rounded-lg" do
      (content_tag :div, class: "px-4 py-5 border-b border-gray-200 sm:px-6" do
        (tag.h3 title, class: "text-lg leading-6 font-medium text-gray-900") +
        (tag.p description, class: "mt-1 max-w-2xl text-sm leading-5 text-gray-500")
      end) +
      (content_tag :div, class: "px-4 py-5 sm:p-0" do
        content_tag :dl do
          capture(&block) if block_given?
        end
      end)
    end
  end

  def description_card_row(name, value, top_border: true)
    border_class = top_border ? "sm:border-t sm:border-gray-200" : ""
    content_tag :div, class: "sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5 #{border_class}" do
      (tag.dt name, class: "text-sm leading-5 font-medium text-gray-500") +
      (tag.dd value, class: "mt-1 text-sm leading-5 text-gray-900 sm:mt-0 sm:col-span-2")
    end
  end

  # TODO Research how to do it with capture and concat. I have not figure it out.
  def description_card_old(title, description, &block)
    capture do
      concat content_tag :div, class: "bg-white shadow overflow-hidden sm:rounded-lg" do
        content_tag :div, class: "px-4 py-5 border-b border-gray-200 sm:px-6" do
          concat tag.h3 title, class: "text-lg leading-6 font-medium text-gray-900"
          concat tag.p description, class: "mt-1 max-w-2xl text-sm leading-5 text-gray-500"
        end
      end
      concat content_tag :div, class: "px-4 py-5 sm:p-0" do
        content_tag :dl do
          # "inside dl".html_safe
          'algo'
        end
      end
    end
  end
end
