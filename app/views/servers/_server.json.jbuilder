json.extract! server, :id, :meta, :provider_id, :created_at, :updated_at
json.url server_url(server, format: :json)
