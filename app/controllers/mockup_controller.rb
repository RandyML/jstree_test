class MockupController < ApplicationController
  def dashboard
  end

  def app_index
  end

  def app_show
  end

  def server_index
  end

  def server_show
  end

  def jstree
  end
end
