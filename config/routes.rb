Rails.application.routes.draw do
  get 'site/index'
  get 'site/docs'
  get 'site/faq'
  get 'site/home'
  get 'site/docs'
  get 'home/dashboard'
  resources :servers
  resources :apps
  get 'mockup/dashboard'
  get 'mockup/app_index'
  get 'mockup/app_show'
  get 'mockup/server_index'
  get 'mockup/server_show'
  get 'mockup/jstree'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'site#index'
end
